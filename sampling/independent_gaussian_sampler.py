from dataclasses import dataclass

from sampling.random_number_sampler_base import ABCRandomNumberSampler, ABCRandomNumberSamplerFactory


@dataclass(frozen=True)
class IncrementingGaussianSamplerParameters:
    initial_value: float
    mu: float
    sigma: float
    sigma_incremental: float


class IncrementingGaussianSampler(ABCRandomNumberSampler):
    _parameters: IncrementingGaussianSamplerParameters
    _current_sigma: float

    def __init__(self, random_number_seed: int, parameters: IncrementingGaussianSamplerParameters) -> None:
        super().__init__(random_number_seed)
        self._parameters = parameters
        self._current_value = self._parameters.initial_value
        self._current_sigma = self._parameters.sigma

    def get_next_value(self) -> float:
        self._current_value = self._parameters.initial_value + self._random_instance.normalvariate(
            self._parameters.mu, self._current_sigma
        )
        self._current_sigma = self._current_sigma + self._parameters.sigma_incremental
        return self._current_value

    def get_initial_value(self) -> float:
        return self._parameters.initial_value


class IndependentGaussianSamplerFactory(ABCRandomNumberSamplerFactory):
    _parameters: IncrementingGaussianSamplerParameters

    def __init__(self, parameters: IncrementingGaussianSamplerParameters):
        self._parameters = parameters

    def create(self, random_number_seed: int) -> IncrementingGaussianSampler:
        return IncrementingGaussianSampler(random_number_seed, self._parameters)
