from dataclasses import dataclass

from sampling.random_number_sampler_base import ABCRandomNumberSampler, ABCRandomNumberSamplerFactory


@dataclass
class CumulativeGaussianSamplerParameters:
    initial_value: float
    mu: float
    sigma: float


class CumulativeGaussianSampler(ABCRandomNumberSampler):
    _current_value: float
    _parameters: CumulativeGaussianSamplerParameters

    def __init__(self, random_number_seed: int, parameters: CumulativeGaussianSamplerParameters) -> None:
        super().__init__(random_number_seed)
        self._parameters = parameters
        self._current_value = self._parameters.initial_value
        self._current_mu = self._parameters.initial_value
        self._current_sigma = self._parameters.sigma

    def get_next_value(self) -> float:
        if self._current_mu < 3.5:
            self._current_mu = min(self._current_mu + self._parameters.mu, 3.5)
        self._current_value = self._random_instance.normalvariate(self._current_mu, self._current_sigma)
        self._current_sigma += 0.0025
        return self._current_value

    def get_initial_value(self) -> float:
        return self._parameters.initial_value


class CumulativeGaussianSamplerFactory(ABCRandomNumberSamplerFactory):
    _parameters: CumulativeGaussianSamplerParameters

    def __init__(self, parameters: CumulativeGaussianSamplerParameters):
        self._parameters = parameters

    def create(self, random_number_seed: int) -> CumulativeGaussianSampler:
        return CumulativeGaussianSampler(random_number_seed, self._parameters)
