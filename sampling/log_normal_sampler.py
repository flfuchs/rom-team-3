from dataclasses import dataclass
from math import exp, log

from sampling.random_number_sampler_base import ABCRandomNumberSampler, ABCRandomNumberSamplerFactory


@dataclass(frozen=True)
class LogNormalLookUpSamplerParameters:
    sigma_initial: float
    sigma_increment: float
    mu: float


class LogNormalLookUpSampler(ABCRandomNumberSampler):
    _current_value: float
    _parameters: LogNormalLookUpSamplerParameters
    _current_lookup_index: int

    def __init__(self, random_number_seed: int, parameters: LogNormalLookUpSamplerParameters) -> None:
        super().__init__(random_number_seed)
        self._parameters = parameters
        self._current_lookup_index = 0

    def get_next_value(self) -> float:
        current_mu = self._parameters.mu
        current_sigma = self._parameters.sigma_initial + self._parameters.sigma_increment * self._current_lookup_index
        self._current_value = exp(self._random_instance.normalvariate(log(current_mu), log(current_sigma)))
        self._current_lookup_index += 1
        return self._current_value

    def get_initial_value(self) -> float:
        return self._parameters.sigma_initial


class LogNormalLookUpSamplerFactory(ABCRandomNumberSamplerFactory):
    _parameters: LogNormalLookUpSamplerParameters

    def __init__(self, parameters: LogNormalLookUpSamplerParameters):
        self._parameters = parameters

    def create(self, random_number_seed: int) -> LogNormalLookUpSampler:
        return LogNormalLookUpSampler(random_number_seed, self._parameters)
