from abc import ABC, abstractmethod
from random import Random


class ABCRandomNumberSampler(ABC):
    _random_instance: Random

    def __init__(self, random_number_seed: int) -> None:
        self._random_instance = Random(random_number_seed)

    @abstractmethod
    def get_next_value(self) -> float:
        pass

    @abstractmethod
    def get_initial_value(self) -> float:
        pass


class ABCRandomNumberSamplerFactory(ABC):
    @abstractmethod
    def create(self, random_number_seed: int) -> ABCRandomNumberSampler:
        pass
