from dataclasses import dataclass

from sampling.random_number_sampler_base import ABCRandomNumberSampler, ABCRandomNumberSamplerFactory


@dataclass
class ConstantSamplerParameters:
    initial_value: int
    deviation: int


class ConstantSampler(ABCRandomNumberSampler):
    _current_value: int
    _parameters: ConstantSamplerParameters

    def __init__(self, random_number_seed: int, parameters: ConstantSamplerParameters) -> None:
        super().__init__(random_number_seed)
        self._parameters = parameters
        self._current_value = self._parameters.initial_value
        self._constant_value = self._random_instance.randrange(-self._parameters.deviation, self._parameters.deviation)

    def get_next_value(self) -> int:
        self._current_value += self._constant_value
        return self._current_value

    def get_initial_value(self) -> int:
        return self._parameters.initial_value


class ConstantSamplerFactory(ABCRandomNumberSamplerFactory):
    _parameters: ConstantSamplerParameters

    def __init__(self, parameters: ConstantSamplerParameters):
        self._parameters = parameters

    def create(self, random_number_seed: int) -> ConstantSampler:
        return ConstantSampler(random_number_seed, self._parameters)
