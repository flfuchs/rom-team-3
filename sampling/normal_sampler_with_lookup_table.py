from dataclasses import dataclass
from typing import Tuple

from sampling.random_number_sampler_base import ABCRandomNumberSampler, ABCRandomNumberSamplerFactory


@dataclass(frozen=True)
class LookupTableSamplerParameters:
    sigma_initial: float
    sigma_increment: float
    expected_mean_values: Tuple[float, ...]


class NormalLookUpSampler(ABCRandomNumberSampler):
    _current_value: float
    _current_lookup_index: int
    _parameters: LookupTableSamplerParameters

    def __init__(self, random_number_seed: int, parameters: LookupTableSamplerParameters) -> None:
        super().__init__(random_number_seed)
        self._parameters = parameters
        self._current_lookup_index = 0

    def get_next_value(self) -> float:
        self._current_lookup_index += 1
        if len(self._parameters.expected_mean_values) > self._current_lookup_index:
            current_mu = self._parameters.expected_mean_values[self._current_lookup_index]
        else:
            current_mu = self._parameters.expected_mean_values[-1]
        current_sigma = self._parameters.sigma_initial + self._parameters.sigma_increment * self._current_lookup_index
        self._current_value = self._random_instance.normalvariate(current_mu, current_sigma)
        return self._current_value

    def get_initial_value(self) -> float:
        return self._parameters.expected_mean_values[0]


class NormalLookUpSamplerFactory(ABCRandomNumberSamplerFactory):
    _parameters: LookupTableSamplerParameters

    def __init__(self, parameters: LookupTableSamplerParameters):
        self._parameters = parameters

    def create(self, random_number_seed: int) -> NormalLookUpSampler:
        return NormalLookUpSampler(random_number_seed, self._parameters)
