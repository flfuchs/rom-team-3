from dataclasses import dataclass

from sampling.random_number_sampler_base import ABCRandomNumberSampler, ABCRandomNumberSamplerFactory


@dataclass
class UniformSamplerParameters:
    initial_value: int
    bias: int
    deviation: int


class UniformSampler(ABCRandomNumberSampler):
    _current_value: int
    _parameters: UniformSamplerParameters

    def __init__(self, random_number_seed: int, parameters: UniformSamplerParameters) -> None:
        super().__init__(random_number_seed)
        self._parameters = parameters
        self._current_value = self._parameters.initial_value

    def get_next_value(self) -> int:
        lower_bound = -self._parameters.deviation + self._parameters.bias
        upper_bound = self._parameters.deviation + self._parameters.bias
        self._current_value += self._random_instance.randrange(lower_bound, upper_bound)
        return self._current_value

    def get_initial_value(self) -> int:
        return self._parameters.initial_value


class UniformSamplerFactory(ABCRandomNumberSamplerFactory):
    _parameters: UniformSamplerParameters

    def __init__(self, parameters: UniformSamplerParameters):
        self._parameters = parameters

    def create(self, random_number_seed: int) -> UniformSampler:
        return UniformSampler(random_number_seed, self._parameters)
