from typing import List, Optional

from evaluation.constant_design_parameters import get_constant_design_parameters
from evaluation.cost_calculations import (
    calculate_discounted_adaptation_construction_cost,
    calculate_discounted_conventional_heating_and_cooling_cost_at_year_i,
    calculate_discounted_renewable_heating_and_cooling_cost_at_year_i,
    calculate_initial_construction_cost_with_bars,
    estimate_if_adaptation_roi_is_positive,
)
from evaluation.dataclasses.design_evaluation_abc import ABCDesignEvaluator
from evaluation.dataclasses.design_parameters import FlexibleDesignParameters
from evaluation.dataclasses.evaluated_design import EvaluatedDesign, YearlyEvaluatedDesign
from simulation.simulated_time_series import ScenarioOfYearlyTimeSeries, YearlyTimeSeries


class ConventionalToHybridDesignEvaluator(ABCDesignEvaluator):
    _design_parameters: FlexibleDesignParameters

    def __init__(self, design_parameters: FlexibleDesignParameters):
        super().__init__(design_parameters)
        self._maximal_number_of_bars = get_constant_design_parameters().MAXIMAL_NUMBER_OF_BARS

    def _design_has_to_be_adapted(
            self,
            time_series: YearlyTimeSeries,
            average_values: YearlyTimeSeries,
            adaptation_index: int,
    ) -> bool:
        return estimate_if_adaptation_roi_is_positive(
            self._design_parameters,
            time_series,
            average_values,
            adaptation_index,
            adaptation_index,
            pump_already_installed=False,
        )

    def _calculate_initial_construction_cost(self) -> float:
        return calculate_initial_construction_cost_with_bars(self._design_parameters, add_pump=False)

    def _calculate_discounted_conventional_heating_and_cooling_cost_at_year_i(
            self, time_series: YearlyTimeSeries, index: int
    ) -> float:
        return calculate_discounted_conventional_heating_and_cooling_cost_at_year_i(
            self._design_parameters, time_series, index
        )

    def _calculate_discounted_adaptation_construction_cost(self, year_index: int) -> float:
        return calculate_discounted_adaptation_construction_cost(self._design_parameters, year_index, add_pump=True)

    def _calculate_discounted_renewable_heating_and_cooling_cost_at_year_i(
            self, time_series: YearlyTimeSeries, index: int, year_of_design_adaptation: int
    ) -> float:
        return calculate_discounted_renewable_heating_and_cooling_cost_at_year_i(
            self._design_parameters, time_series, index, year_of_design_adaptation
        )

    def evaluate_design(self, yearly_time_series: ScenarioOfYearlyTimeSeries) -> EvaluatedDesign:
        evaluations = []
        for time_series in yearly_time_series.time_series:
            discounted_cost: List[float] = [self._calculate_initial_construction_cost()]
            index_of_design_adaptation: Optional[int] = None
            for year, index in enumerate(yearly_time_series.year_index[1:], start=1):
                is_constructed = index > self._design_parameters.construction_time_in_years
                if is_constructed:
                    design_is_adapted = index_of_design_adaptation is not None
                    if design_is_adapted:
                        renewable_cost = self._calculate_discounted_renewable_heating_and_cooling_cost_at_year_i(
                            time_series, index, index_of_design_adaptation
                        )
                        conventional_cost = self._calculate_discounted_conventional_heating_and_cooling_cost_at_year_i(
                            time_series, index
                        )
                        hybrid_split = self._design_parameters.number_of_bars / self._maximal_number_of_bars
                        discounted_cost.append(hybrid_split * renewable_cost + conventional_cost * (1 - hybrid_split))
                    else:
                        conventional_cost = self._calculate_discounted_conventional_heating_and_cooling_cost_at_year_i(
                            time_series, index
                        )

                        if self._design_has_to_be_adapted(time_series, yearly_time_series.average_values, index):
                            index_of_design_adaptation = index
                            adaptation_cost = self._calculate_discounted_adaptation_construction_cost(index)
                            discounted_cost.append(adaptation_cost + conventional_cost)
                        else:
                            discounted_cost.append(conventional_cost)
                else:
                    discounted_cost.append(0)
            evaluations.append(YearlyEvaluatedDesign(tuple(discounted_cost), index_of_design_adaptation))

        return EvaluatedDesign(yearly_time_series.year_index, tuple(evaluations))


def create_conventional_to_hybrid_design_evaluator(max_time_in_years_for_adaptation_roi: int):
    constant_parameters = get_constant_design_parameters()
    parameters = FlexibleDesignParameters(
        discount_rate=constant_parameters.DISCOUNT_RATE,
        initial_construction_cost=constant_parameters.INVESTMENT_FOR_CONVENTIONAL_HEATING,
        construction_time_in_years=constant_parameters.INITIAL_CONSTRUCTION_TIME_IN_YEARS,
        required_power_to_heat_one_hour=constant_parameters.REQUIRED_POWER_TO_HEAT_ONE_HOUR,
        required_power_to_cool_one_hour=constant_parameters.REQUIRED_POWER_TO_COOL_ONE_HOUR,
        heat_pump_power=constant_parameters.HEAT_PUMP_POWER,
        number_of_bars=constant_parameters.MAXIMAL_NUMBER_OF_BARS * 0.4,
        bar_investment_cost=constant_parameters.BAR_INVESTMENT_COST,
        pump_investment_cost=constant_parameters.PUMP_INVESTMENT_COST * 0.4,
        renewable_heat_incentives=constant_parameters.RENEWABLE_HEAT_INCENTIVES,
        max_time_in_years_for_adaptation_roi=max_time_in_years_for_adaptation_roi,
        ratio_of_initially_drilled_bars=0.0,
    )
    return ConventionalToHybridDesignEvaluator(parameters)


def create_conventional_to_hybrid_design_evaluator_complete_pump_cost(max_time_in_years_for_adaptation_roi: int):
    constant_parameters = get_constant_design_parameters()
    parameters = FlexibleDesignParameters(
        discount_rate=constant_parameters.DISCOUNT_RATE,
        initial_construction_cost=constant_parameters.INVESTMENT_FOR_CONVENTIONAL_HEATING,
        construction_time_in_years=constant_parameters.INITIAL_CONSTRUCTION_TIME_IN_YEARS,
        required_power_to_heat_one_hour=constant_parameters.REQUIRED_POWER_TO_HEAT_ONE_HOUR,
        required_power_to_cool_one_hour=constant_parameters.REQUIRED_POWER_TO_COOL_ONE_HOUR,
        heat_pump_power=constant_parameters.HEAT_PUMP_POWER,
        number_of_bars=constant_parameters.MAXIMAL_NUMBER_OF_BARS * 0.4,  # only 40 % of all bars allowed
        bar_investment_cost=constant_parameters.BAR_INVESTMENT_COST,
        pump_investment_cost=constant_parameters.PUMP_INVESTMENT_COST,
        renewable_heat_incentives=constant_parameters.RENEWABLE_HEAT_INCENTIVES,
        max_time_in_years_for_adaptation_roi=max_time_in_years_for_adaptation_roi,
        ratio_of_initially_drilled_bars=0.0,
    )
    return ConventionalToHybridDesignEvaluator(parameters)
