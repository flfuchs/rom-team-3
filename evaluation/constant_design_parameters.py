from dataclasses import dataclass, field

_discount_rate = None


def set_discount_rate(rate: float) -> None:
    global _discount_rate
    _discount_rate = rate
    print(_discount_rate)


@dataclass(frozen=True, init=False)
class _ConstantDesignParameters:
    DISCOUNT_RATE: float = field(metadata=dict(unit=None))
    INITIAL_CONSTRUCTION_TIME_IN_YEARS: int = field(default=3, metadata=dict(unit="Years"))
    REQUIRED_POWER_TO_HEAT_ONE_HOUR: float = field(default=200, metadata=dict(unit="Kwh"))
    REQUIRED_POWER_TO_COOL_ONE_HOUR: float = field(default=150, metadata=dict(unit="Kwh"))
    HEAT_PUMP_POWER: float = field(default=200, metadata=dict(unit="Kw"))
    BAR_INVESTMENT_COST: float = field(default=600, metadata=dict(unit="Pounds"))
    PUMP_INVESTMENT_COST: float = field(default=600_000, metadata=dict(unit="Pounds"))
    RENEWABLE_HEAT_INCENTIVES: float = field(default=0.09, metadata=dict(unit="Pounds/Kwh"))
    MAXIMAL_NUMBER_OF_BARS: int = field(default=200, metadata=dict(unit="count"))
    INVESTMENT_FOR_CONVENTIONAL_HEATING: int = field(default=167_000, metadata=dict(unit="Pounds"))

    def __init__(self, rate: float):
        object.__setattr__(self, "DISCOUNT_RATE", rate)


def get_constant_design_parameters() -> _ConstantDesignParameters:
    return _ConstantDesignParameters(_discount_rate)
