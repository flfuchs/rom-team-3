from dataclasses import dataclass

from evaluation.dataclasses.design_evaluation_abc import ABCDesignParameters


@dataclass(frozen=True)
class FlexibleDesignParameters(ABCDesignParameters):
    discount_rate: float
    initial_construction_cost: float
    construction_time_in_years: int
    required_power_to_heat_one_hour: float
    required_power_to_cool_one_hour: float
    heat_pump_power: float
    number_of_bars: float
    bar_investment_cost: float
    pump_investment_cost: float
    renewable_heat_incentives: float
    max_time_in_years_for_adaptation_roi: int
    ratio_of_initially_drilled_bars: float


class ConventionalDesignParameters(ABCDesignParameters):
    pass
