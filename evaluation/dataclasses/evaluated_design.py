from dataclasses import dataclass
from typing import Tuple, Optional


@dataclass(frozen=True)
class YearlyEvaluatedDesign:
    __slots__ = ["evaluation_value", "design_switch_triggered_at"]
    evaluation_value: Tuple[float, ...]
    design_switch_triggered_at: Optional[int]

    def __getstate__(self):
        return dict((name, getattr(self, name)) for name in self.__slots__)

    def __setstate__(self, state):
        for name, value in state.items():
            object.__setattr__(self, name, value)


@dataclass(frozen=True)
class EvaluatedDesign:
    year_index: Tuple[int, ...]
    evaluated_time_series: Tuple[YearlyEvaluatedDesign, ...]
