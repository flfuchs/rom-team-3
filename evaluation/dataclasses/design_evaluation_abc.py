from abc import ABC, abstractmethod
from dataclasses import dataclass

from evaluation.dataclasses.evaluated_design import EvaluatedDesign
from simulation.simulated_time_series import ScenarioOfYearlyTimeSeries


@dataclass(frozen=True)
class ABCDesignParameters(ABC):
    discount_rate: float
    initial_construction_cost: float
    construction_time_in_years: int
    required_power_to_heat_one_hour: float
    required_power_to_cool_one_hour: float


class ABCDesignEvaluator(ABC):
    _design_parameters: ABCDesignParameters

    def __init__(self, design_parameters: ABCDesignParameters):
        self._design_parameters = design_parameters

    @abstractmethod
    def evaluate_design(self, completed_simulation_run: ScenarioOfYearlyTimeSeries) -> EvaluatedDesign:
        pass
