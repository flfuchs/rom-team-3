def create_label_for_evaluated_simulation_run(evaluator_name: str) -> str:
    return f"evaluated with {evaluator_name}"
