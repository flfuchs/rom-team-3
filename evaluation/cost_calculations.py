from evaluation.constant_design_parameters import get_constant_design_parameters
from evaluation._convenience import discount_value_by_time
from evaluation.dataclasses.design_evaluation_abc import ABCDesignParameters
from evaluation.dataclasses.design_parameters import FlexibleDesignParameters
from simulation.simulated_time_series import YearlyTimeSeries


def _calculate_conventional_heating_and_cooling_cost(
        design_parameters: ABCDesignParameters, year_index: int, time_series: YearlyTimeSeries
) -> float:
    heating_cost = (
            time_series.hours_of_heating[year_index]
            * time_series.average_gas_price[year_index]
            * design_parameters.required_power_to_heat_one_hour
    )
    cooling_cost = (
            time_series.hours_of_cooling[year_index]
            * time_series.average_electricity_price[year_index]
            * design_parameters.required_power_to_cool_one_hour
    )
    return cooling_cost + heating_cost


def calculate_discounted_conventional_heating_and_cooling_cost_at_year_i(
        design_parameters: ABCDesignParameters, time_series: YearlyTimeSeries, i: int
) -> float:
    cost = _calculate_conventional_heating_and_cooling_cost(design_parameters, i, time_series)
    return discount_value_by_time(design_parameters.discount_rate, cost, i)


def calculate_initial_construction_cost_with_bars(design_parameters: FlexibleDesignParameters, add_pump: bool) -> float:
    number_of_bars = design_parameters.number_of_bars * design_parameters.ratio_of_initially_drilled_bars
    cost_to_drill_some_bars = number_of_bars * design_parameters.bar_investment_cost
    if add_pump:
        return (
                design_parameters.initial_construction_cost
                + cost_to_drill_some_bars
                + design_parameters.pump_investment_cost
        )
    else:
        return design_parameters.initial_construction_cost + cost_to_drill_some_bars


def calculate_discounted_adaptation_construction_cost(
        design_parameters: FlexibleDesignParameters,
        year_index: int,
        add_pump: bool,
) -> float:
    investment_cost = _calculate_adaptation_construction_cost(add_pump, design_parameters)
    return discount_value_by_time(design_parameters.discount_rate, investment_cost, year_index)


def _calculate_adaptation_construction_cost(add_pump: bool, design_parameters: FlexibleDesignParameters) -> float:
    number_of_bars_to_add = design_parameters.number_of_bars * (1 - design_parameters.ratio_of_initially_drilled_bars)
    if add_pump:
        return design_parameters.bar_investment_cost * number_of_bars_to_add + design_parameters.pump_investment_cost

    else:
        return design_parameters.bar_investment_cost * number_of_bars_to_add


def calculate_discounted_renewable_heating_and_cooling_cost_at_year_i(
        design_parameters: FlexibleDesignParameters,
        time_series: YearlyTimeSeries,
        i: int,
        year_of_design_adaptation: int,
) -> float:
    total_cost = _calculate_renewable_heating_and_cooling_cost_at_year_i(
        design_parameters, time_series, i, time_series.heat_pump_coefficient_of_performance[year_of_design_adaptation]
    )
    return discount_value_by_time(design_parameters.discount_rate, total_cost, i)


def _calculate_renewable_heating_and_cooling_cost_at_year_i(
        design_parameters: FlexibleDesignParameters,
        time_series: YearlyTimeSeries,
        year_index: int,
        constant_heat_pump_coefficient_of_performance: float,
):
    heating_cost = (
            time_series.hours_of_heating[year_index]
            * time_series.average_electricity_price[year_index]
            * design_parameters.required_power_to_heat_one_hour
            / constant_heat_pump_coefficient_of_performance
    )
    heating_incentives = (
            time_series.hours_of_heating[year_index]
            * design_parameters.required_power_to_heat_one_hour
            * design_parameters.renewable_heat_incentives
    )
    cooling_cost = (
            time_series.hours_of_cooling[year_index]
            * time_series.average_electricity_price[year_index]
            * design_parameters.required_power_to_cool_one_hour
            / constant_heat_pump_coefficient_of_performance
    )
    cooling_incentives = (
            time_series.hours_of_cooling[year_index]
            * design_parameters.required_power_to_cool_one_hour
            * design_parameters.renewable_heat_incentives
    )
    return heating_cost - heating_incentives + cooling_cost - cooling_incentives


def estimate_if_adaptation_roi_is_positive(
        design_parameters: FlexibleDesignParameters,
        time_series: YearlyTimeSeries,
        average_values: YearlyTimeSeries,
        adaptation_index: int,
        current_index: int,
        pump_already_installed: bool,
) -> bool:
    time_span_for_roi = design_parameters.max_time_in_years_for_adaptation_roi
    if len(average_values.average_gas_price) < current_index + 1 + time_span_for_roi:
        pass
    time_range = range(
        adaptation_index, min(len(average_values.average_gas_price), adaptation_index + time_span_for_roi)
    )
    adaptation_cost = _calculate_adaptation_construction_cost(not pump_already_installed, design_parameters)
    renewable_cost = [
        _calculate_renewable_heating_and_cooling_cost_at_year_i(
            design_parameters,
            average_values,
            index,
            time_series.heat_pump_coefficient_of_performance[adaptation_index],
        )
        for index in time_range
    ]
    conventional_cost = [
        _calculate_conventional_heating_and_cooling_cost(design_parameters, index, average_values)
        for index in time_range
    ]
    if pump_already_installed:
        current_design_cost = sum(
            discount_value_by_time(
                design_parameters.discount_rate,
                r_n * design_parameters.ratio_of_initially_drilled_bars
                + r_c * (1 - design_parameters.ratio_of_initially_drilled_bars),
                year,
            )
            for year, (r_n, r_c) in enumerate(zip(renewable_cost, conventional_cost))
        )
    else:
        current_design_cost = sum(
            discount_value_by_time(design_parameters.discount_rate, cost, year)
            for year, cost in enumerate(conventional_cost)
        )

    hybrid_split = design_parameters.number_of_bars / get_constant_design_parameters().MAXIMAL_NUMBER_OF_BARS
    if hybrid_split == 1:
        discounted_renewable_cost = sum(
            discount_value_by_time(design_parameters.discount_rate, c, i) for i, c in enumerate(renewable_cost)
        )
    else:
        discounted_renewable_cost = sum(
            discount_value_by_time(
                design_parameters.discount_rate,
                c_r * hybrid_split + (1 - hybrid_split) * c_c,
                i
            ) for i, (c_r, c_c) in enumerate(zip(renewable_cost, conventional_cost))
        )

    return (discounted_renewable_cost + adaptation_cost) < current_design_cost
