from typing import List

from evaluation.constant_design_parameters import get_constant_design_parameters
from evaluation.cost_calculations import (
    calculate_discounted_renewable_heating_and_cooling_cost_at_year_i,
    calculate_initial_construction_cost_with_bars,
)
from evaluation.dataclasses.design_evaluation_abc import ABCDesignEvaluator
from evaluation.dataclasses.design_parameters import FlexibleDesignParameters
from evaluation.dataclasses.evaluated_design import EvaluatedDesign, YearlyEvaluatedDesign
from simulation.simulated_time_series import ScenarioOfYearlyTimeSeries, YearlyTimeSeries


class AlternativeDesignEvaluator(ABCDesignEvaluator):
    _design_parameters: FlexibleDesignParameters

    def __init__(self, design_parameters: FlexibleDesignParameters):
        super().__init__(design_parameters)

    def _calculate_heating_and_cooling_cost_at_year_i(self, time_series: YearlyTimeSeries, i: int) -> float:
        year_of_design_adaptation = 1
        return calculate_discounted_renewable_heating_and_cooling_cost_at_year_i(
            self._design_parameters, time_series, i, year_of_design_adaptation
        )

    def _calculate_initial_construction_cost(self) -> float:
        return calculate_initial_construction_cost_with_bars(self._design_parameters, add_pump=True)

    def evaluate_design(self, yearly_time_series: ScenarioOfYearlyTimeSeries) -> EvaluatedDesign:
        evaluations = []
        for time_series in yearly_time_series.time_series:
            discounted_cost: List[float] = [self._calculate_initial_construction_cost()]
            for year, index in enumerate(yearly_time_series.year_index[1:], start=1):
                is_constructed = index > self._design_parameters.construction_time_in_years
                if is_constructed:
                    discounted_cost.append(self._calculate_heating_and_cooling_cost_at_year_i(time_series, index))
                else:
                    discounted_cost.append(0)
            evaluations.append(YearlyEvaluatedDesign(tuple(discounted_cost), None))

        return EvaluatedDesign(yearly_time_series.year_index, tuple(evaluations))


def create_alternative_design_evaluator():
    constant_parameters = get_constant_design_parameters()
    parameters = FlexibleDesignParameters(
        discount_rate=constant_parameters.DISCOUNT_RATE,
        initial_construction_cost=constant_parameters.INVESTMENT_FOR_CONVENTIONAL_HEATING * 0,
        construction_time_in_years=constant_parameters.INITIAL_CONSTRUCTION_TIME_IN_YEARS,
        required_power_to_heat_one_hour=constant_parameters.REQUIRED_POWER_TO_HEAT_ONE_HOUR,
        required_power_to_cool_one_hour=constant_parameters.REQUIRED_POWER_TO_COOL_ONE_HOUR,
        heat_pump_power=constant_parameters.HEAT_PUMP_POWER,
        number_of_bars=constant_parameters.MAXIMAL_NUMBER_OF_BARS,
        bar_investment_cost=constant_parameters.BAR_INVESTMENT_COST,
        pump_investment_cost=constant_parameters.PUMP_INVESTMENT_COST,
        renewable_heat_incentives=constant_parameters.RENEWABLE_HEAT_INCENTIVES,
        max_time_in_years_for_adaptation_roi=1,
        ratio_of_initially_drilled_bars=1,
    )
    return AlternativeDesignEvaluator(parameters)
