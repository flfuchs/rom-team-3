from math import exp

from numba import njit


@njit
def discount_value_by_time(discount_rate: float, cost: float, time_step: float) -> float:
    discounted_cost = exp(-(discount_rate * time_step)) * cost
    return discounted_cost
