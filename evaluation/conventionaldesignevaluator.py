from typing import List

from evaluation.constant_design_parameters import get_constant_design_parameters
from evaluation.cost_calculations import calculate_discounted_conventional_heating_and_cooling_cost_at_year_i
from evaluation.dataclasses.design_evaluation_abc import ABCDesignEvaluator
from evaluation.dataclasses.design_parameters import ConventionalDesignParameters
from evaluation.dataclasses.evaluated_design import EvaluatedDesign, YearlyEvaluatedDesign
from simulation.simulated_time_series import ScenarioOfYearlyTimeSeries, YearlyTimeSeries


class ConventionalDesignEvaluator(ABCDesignEvaluator):
    _design_parameters: ConventionalDesignParameters

    def __init__(self, design_parameters: ConventionalDesignParameters):
        super().__init__(design_parameters)

    def _calculate_heating_and_cooling_cost_at_year_i(self, time_series: YearlyTimeSeries, i: int) -> float:
        return calculate_discounted_conventional_heating_and_cooling_cost_at_year_i(
            self._design_parameters, time_series, i
        )

    def evaluate_design(self, yearly_time_series: ScenarioOfYearlyTimeSeries) -> EvaluatedDesign:
        evaluations = []
        for time_series in yearly_time_series.time_series:
            discounted_cost: List[float] = [self._design_parameters.initial_construction_cost]
            for year, index in enumerate(yearly_time_series.year_index[1:], start=1):
                is_constructed = index > self._design_parameters.construction_time_in_years
                if is_constructed:
                    discounted_cost.append(self._calculate_heating_and_cooling_cost_at_year_i(time_series, index))
                else:
                    discounted_cost.append(0)
            evaluations.append(YearlyEvaluatedDesign(tuple(discounted_cost), None))

        return EvaluatedDesign(yearly_time_series.year_index, tuple(evaluations))


def create_conventional_design_evaluator():
    constant_parameters = get_constant_design_parameters()
    parameters = ConventionalDesignParameters(
        discount_rate=constant_parameters.DISCOUNT_RATE,
        initial_construction_cost=constant_parameters.INVESTMENT_FOR_CONVENTIONAL_HEATING,
        construction_time_in_years=constant_parameters.INITIAL_CONSTRUCTION_TIME_IN_YEARS,
        required_power_to_heat_one_hour=constant_parameters.REQUIRED_POWER_TO_HEAT_ONE_HOUR,
        required_power_to_cool_one_hour=constant_parameters.REQUIRED_POWER_TO_COOL_ONE_HOUR,
    )
    return ConventionalDesignEvaluator(parameters)
