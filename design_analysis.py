import multiprocessing
import os
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from statistics import stdev
from typing import Iterable, List, Tuple

import numpy as np

from evaluation.constant_design_parameters import set_discount_rate
from evaluation.alternaivedesignevaluator import create_alternative_design_evaluator
from evaluation.conventionaldesignevaluator import create_conventional_design_evaluator
from evaluation.conventionaltohyrbiddesignevaluator import (
    create_conventional_to_hybrid_design_evaluator,
    create_conventional_to_hybrid_design_evaluator_complete_pump_cost,
)
from evaluation.dataclasses.evaluated_design import EvaluatedDesign
from evaluation.flexibledesignevaluator import create_flexible_design_evaluator
from plotting.plot_simulation_runs import plot_design_switch_vs_time_per_design, plot_cumulated_values_vs_probabilities
from plotting.plot_yearly_time_series import plot_probability_intervals
from simulation.configuration import create_simulation_configuration
from simulation.monte_carlo_simulator_for_yearly_time_series import MonteCarloSimulatorForYearlyTimeSeries
from simulation.simulated_time_series import ScenarioOfYearlyTimeSeries


def keep_design_with_minimum_cost(
        labels: List[str], evaluated_designs: List[EvaluatedDesign]
) -> Tuple[Tuple[str], Tuple[EvaluatedDesign]]:
    i_min = np.argmin(
        (
            np.fromiter(
                (
                    np.mean(np.fromiter((np.sum(run.evaluation_value) for run in design.evaluated_time_series), float))
                    for design in evaluated_designs
                ),
                float,
            )
        )
    )
    return (labels[i_min],), (evaluated_designs[i_min],)


def evaluate_conventional_to_smaller_hybrid_design_with_ranging_trigger(
        simulations: ScenarioOfYearlyTimeSeries, trigger_range: Iterable[int]
) -> Tuple[List[str], List[EvaluatedDesign]]:
    with ProcessPoolExecutor(max_workers=int(os.cpu_count() / 2)) as executor:
        results = []
        for trigger_value in trigger_range:
            partial_flexible_design = create_conventional_to_hybrid_design_evaluator_complete_pump_cost(
                max_time_in_years_for_adaptation_roi=trigger_value
            )
            results.append(executor.submit(partial_flexible_design.evaluate_design, simulations))
        partial_flexible_labels = [
            f"conventional to hybrid, trigger at {trigger_value}" if trigger_value > 0 else
            "conventional to hybrid, never triggered"
            for trigger_value in trigger_range
        ]
        flexible_results = [job.result() for job in results]
    return partial_flexible_labels, flexible_results


def evaluate_flexible_design_with_ranging_trigger(
        simulations: ScenarioOfYearlyTimeSeries, trigger_range: Iterable[int]
) -> Tuple[List[str], List[EvaluatedDesign]]:
    with ProcessPoolExecutor(
            max_workers=int(os.cpu_count() / 2),
    ) as executor:
        flexible_scheduled_jobs = []
        for trigger_value in trigger_range:
            flexible_design = create_flexible_design_evaluator(max_time_in_years_for_adaptation_roi=trigger_value)
            flexible_scheduled_jobs.append(executor.submit(flexible_design.evaluate_design, simulations))
        flexible_labels = [
            f"conventional to alternative, trigger at {trigger_value}" if trigger_value > 0 else
            "conventional to alternative, never triggered"
            for trigger_value in trigger_range
        ]
        flexible_results: List[EvaluatedDesign] = [job.result() for job in flexible_scheduled_jobs]
    return flexible_labels, flexible_results


def evaluate_conventional_to_hybrid_design_with_ranging_trigger(
        simulations: ScenarioOfYearlyTimeSeries, trigger_range: Iterable[int]
) -> Tuple[List[str], List[EvaluatedDesign]]:
    with ProcessPoolExecutor(
            max_workers=int(os.cpu_count() / 2),
    ) as executor:
        flexible_scheduled_jobs = []
        for trigger_value in trigger_range:
            flexible_design = create_conventional_to_hybrid_design_evaluator(
                max_time_in_years_for_adaptation_roi=trigger_value
            )
            flexible_scheduled_jobs.append(executor.submit(flexible_design.evaluate_design, simulations))
        labels = [f"conventional to smaller hybrid, trigger at {trigger_value}" if trigger_value > 0 else
                  "conventional to smaller hybrid, never triggered"
                  for trigger_value in trigger_range
                  ]
        results = [job.result() for job in flexible_scheduled_jobs]
    return labels, results


def main() -> None:
    configuration = create_simulation_configuration()

    monte_carlo_simulator = MonteCarloSimulatorForYearlyTimeSeries.create_from_simulation_configuration(configuration)
    simulations = monte_carlo_simulator.simulate_yearly_time_series(
        number_of_simulations=configuration.number_of_simulations, number_of_years=configuration.number_of_years
    )

    multiprocessing.Process(
        target=plot_probability_intervals, args=(simulations, ["hours_of_cooling", "hours_of_heating"])
    ).start()
    multiprocessing.Process(
        target=plot_probability_intervals, args=(simulations, ["average_electricity_price", "average_gas_price"])
    ).start()
    multiprocessing.Process(
        target=plot_probability_intervals, args=(simulations, ["heat_pump_coefficient_of_performance"])
    ).start()

    for discount_rate in configuration.discount_values:
        set_discount_rate(discount_rate)
        conventional_design = create_conventional_design_evaluator()
        alternative_design = create_alternative_design_evaluator()

        fixed_results = []
        for design in (conventional_design, alternative_design):
            fixed_results.append(design.evaluate_design(simulations))
        fixed_labels = ("regular", "alternative")

        trigger_range = range(0, 40, 5)
        with ThreadPoolExecutor(max_workers=3) as pool:
            full_flexible_job = pool.submit(evaluate_flexible_design_with_ranging_trigger, simulations, trigger_range)
            partial_job = pool.submit(evaluate_conventional_to_smaller_hybrid_design_with_ranging_trigger, simulations,
                                      trigger_range)
            hybrid_job = pool.submit(
                evaluate_conventional_to_hybrid_design_with_ranging_trigger, simulations, trigger_range
            )

            flexible_labels, flexible_results = keep_design_with_minimum_cost(*full_flexible_job.result())
            partial_flexible_labels, partial_flexible_results = keep_design_with_minimum_cost(*partial_job.result())
            hybrid_labels, hybrid_results = keep_design_with_minimum_cost(*hybrid_job.result())

        evaluation_results = tuple(fixed_results) + flexible_results + partial_flexible_results + hybrid_results

        labels = fixed_labels + flexible_labels + partial_flexible_labels + hybrid_labels
        figure = plot_cumulated_values_vs_probabilities(evaluation_results, labels)
        if not os.path.exists("out"):
            os.mkdir("out")
        figure.write_html(os.path.join("out", f"all_evaluations_with_discount_rate_{discount_rate}.html"))
        figure.show()
        figure = plot_design_switch_vs_time_per_design(evaluation_results, labels)
        figure.write_html(
            os.path.join("out", f"all_design_switch_vs_time_per_design_with_discount_rate_{discount_rate}.html"))
        figure.show()
        for design, label in zip(evaluation_results, labels):
            st_dev = stdev(sum(run.evaluation_value) for run in design.evaluated_time_series)
            print(f"{label}: {st_dev} as stdev")


if __name__ == "__main__":
    main()
