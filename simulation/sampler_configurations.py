from dataclasses import dataclass
from typing import Tuple

from sampling.random_number_sampler_base import ABCRandomNumberSampler, ABCRandomNumberSamplerFactory


@dataclass(frozen=True)
class RandomNumberSamplers:
    heating_hour_sampler: ABCRandomNumberSampler
    cooling_hour_sampler: ABCRandomNumberSampler
    gas_cost_sampler: ABCRandomNumberSampler
    electricity_cost_sampler: ABCRandomNumberSampler
    heat_pump_coefficient_of_performance_sampler: ABCRandomNumberSampler


@dataclass(frozen=True)
class RandomNumberSamplerFactories:
    heating_hour_sampler_factory: ABCRandomNumberSamplerFactory
    cooling_hour_sampler_factory: ABCRandomNumberSamplerFactory
    gas_cost_sampler_factory: ABCRandomNumberSamplerFactory
    electricity_cost_sampler_factory: ABCRandomNumberSamplerFactory
    heat_pump_coefficient_of_performance_sampler: ABCRandomNumberSamplerFactory

    def create_random_number_samplers(self, seeds: Tuple[int, int, int, int, int]) -> RandomNumberSamplers:
        return RandomNumberSamplers(
            self.heating_hour_sampler_factory.create(seeds[0]),
            self.cooling_hour_sampler_factory.create(seeds[1]),
            self.gas_cost_sampler_factory.create(seeds[2]),
            self.electricity_cost_sampler_factory.create(seeds[3]),
            self.heat_pump_coefficient_of_performance_sampler.create(seeds[4]),
        )

    @property
    def seed_step_size(self) -> int:
        return 5
