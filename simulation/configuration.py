from dataclasses import dataclass
from typing import Tuple

from sampling.cumulative_gaussian_sampler import CumulativeGaussianSamplerFactory, CumulativeGaussianSamplerParameters
from sampling.independent_gaussian_sampler import (
    IncrementingGaussianSamplerParameters,
    IndependentGaussianSamplerFactory,
)
from sampling.normal_sampler_with_lookup_table import LookupTableSamplerParameters, NormalLookUpSamplerFactory
from simulation.sampler_configurations import RandomNumberSamplerFactories


@dataclass(frozen=True)
class SimulationConfiguration:
    initial_random_seed: int
    number_of_simulations: int
    number_of_years: int
    random_number_sampler_factories: RandomNumberSamplerFactories
    discount_values: Tuple[float, ...]


def create_simulation_configuration() -> SimulationConfiguration:
    return SimulationConfiguration(
        initial_random_seed=1,
        number_of_simulations=1_000,
        number_of_years=50,
        random_number_sampler_factories=RandomNumberSamplerFactories(
            gas_cost_sampler_factory=NormalLookUpSamplerFactory(
                LookupTableSamplerParameters(
                    sigma_initial=0.00401,
                    sigma_increment=0.000_401,
                    expected_mean_values=(
                        0.0401,
                        0.0409,
                        0.0432,
                        0.0447,
                        0.0459,
                        0.0471,
                        0.0482,
                        0.0490,
                        0.0494,
                        0.0502,
                        0.0510,
                        0.0521,
                        0.0525,
                        0.0525,
                        0.0529,
                        0.0533,
                        0.0537,
                        0.0541,
                        0.0543,
                        0.0545,
                        0.0547,
                        0.0549,
                        0.0551,
                        0.0552,
                        0.0554,
                        0.0556,
                        0.0558,
                        0.0560,
                        0.0562,
                        0.0564,
                        0.0566,
                        0.0568,
                        0.0570,
                        0.0572,
                        0.0574,
                        0.0576,
                        0.0578,
                    ),
                )
            ),
            electricity_cost_sampler_factory=NormalLookUpSamplerFactory(
                LookupTableSamplerParameters(
                    sigma_initial=0.019,
                    sigma_increment=0.0019,
                    expected_mean_values=(
                        0.1900,
                        0.2000,
                        0.2100,
                        0.2200,
                        0.2300,
                        0.2350,
                        0.2380,
                        0.2390,
                        0.2400,
                        0.2400,
                        0.2410,
                        0.2430,
                        0.2460,
                        0.2490,
                        0.2495,
                        0.2500,
                        0.2505,
                        0.2510,
                        0.2515,
                        0.2520,
                        0.2525,
                        0.2530,
                        0.2535,
                        0.2540,
                        0.2545,
                        0.2550,
                        0.2555,
                        0.2560,
                        0.2565,
                        0.2570,
                        0.2575,
                        0.2580,
                        0.2585,
                        0.2590,
                        0.2595,
                        0.2600,
                        0.2605,
                    ),
                )
            ),
            cooling_hour_sampler_factory=IndependentGaussianSamplerFactory(
                IncrementingGaussianSamplerParameters(initial_value=700, mu=0, sigma=50, sigma_incremental=2 * 0.5),
            ),
            heating_hour_sampler_factory=IndependentGaussianSamplerFactory(
                IncrementingGaussianSamplerParameters(initial_value=875, mu=0, sigma=62.5, sigma_incremental=2 * 0.625)
            ),
            heat_pump_coefficient_of_performance_sampler=CumulativeGaussianSamplerFactory(
                CumulativeGaussianSamplerParameters(initial_value=2.5, mu=0.04, sigma=0.25)
            ),
        ),
        discount_values=(0.03, 0.005,  0.06)
    )
