from __future__ import annotations

from dataclasses import fields
from statistics import mean
from typing import Tuple

from sampling.random_number_sampler_base import ABCRandomNumberSampler
from simulation.configuration import SimulationConfiguration
from simulation.sampler_configurations import RandomNumberSamplerFactories, RandomNumberSamplers
from simulation.simulated_time_series import ScenarioOfYearlyTimeSeries, YearlyTimeSeries


def _sample_for_number_of_years(sampler: ABCRandomNumberSampler, number_of_years: int) -> Tuple[float, ...]:
    return tuple([sampler.get_initial_value()] + [sampler.get_next_value() for _ in range(number_of_years)])


def _calculate_yearly_average_per_attribute(simulated_evolutions: Tuple[YearlyTimeSeries, ...]) -> YearlyTimeSeries:
    average_values = {
        str(field.name): [
            mean(
                simulated_evolutions[run].__getattribute__(field.name)[year_index]
                for run in range(len(simulated_evolutions))
            )
            for year_index in range(len(simulated_evolutions[0].average_gas_price))
        ]
        for field in fields(YearlyTimeSeries)
    }
    return YearlyTimeSeries(**average_values)


class MonteCarloSimulatorForYearlyTimeSeries:
    _random_seed: int
    _random_number_sampler_factories: RandomNumberSamplerFactories

    def __init__(self, initial_random_seed: int, sampler_factories: RandomNumberSamplerFactories):
        self._random_seed = initial_random_seed
        self._random_number_sampler_factories = sampler_factories

    @classmethod
    def create_from_simulation_configuration(
        cls, configuration: SimulationConfiguration
    ) -> MonteCarloSimulatorForYearlyTimeSeries:
        return cls(
            initial_random_seed=configuration.initial_random_seed,
            sampler_factories=configuration.random_number_sampler_factories,
        )

    def _create_next_set_of_samplers(self) -> RandomNumberSamplers:
        seed_step_size = self._random_number_sampler_factories.seed_step_size
        seeds: Tuple[int, int, int, int, int] = tuple(range(self._random_seed, self._random_seed + seed_step_size))
        self._random_seed += seed_step_size
        return self._random_number_sampler_factories.create_random_number_samplers(seeds)

    def _simulate_one_time_series(self, number_of_years: int) -> YearlyTimeSeries:
        samplers = self._create_next_set_of_samplers()
        return YearlyTimeSeries(
            hours_of_cooling=_sample_for_number_of_years(samplers.cooling_hour_sampler, number_of_years),
            hours_of_heating=_sample_for_number_of_years(samplers.heating_hour_sampler, number_of_years),
            average_gas_price=_sample_for_number_of_years(samplers.gas_cost_sampler, number_of_years),
            average_electricity_price=_sample_for_number_of_years(samplers.electricity_cost_sampler, number_of_years),
            heat_pump_coefficient_of_performance=_sample_for_number_of_years(
                samplers.heat_pump_coefficient_of_performance_sampler, number_of_years
            ),
        )

    def simulate_yearly_time_series(
        self, number_of_years: int, number_of_simulations: int
    ) -> ScenarioOfYearlyTimeSeries:
        time_series = tuple(self._simulate_one_time_series(number_of_years) for _ in range(number_of_simulations))
        return ScenarioOfYearlyTimeSeries(
            year_index=tuple(i for i in range(number_of_years + 1)),
            time_series=time_series,
            average_values=_calculate_yearly_average_per_attribute(time_series),
        )
