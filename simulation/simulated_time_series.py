from dataclasses import dataclass
from typing import Tuple


@dataclass(frozen=True)
class YearlyTimeSeries:
    __slots__ = (
        "hours_of_cooling",
        "hours_of_heating",
        "average_gas_price",
        "average_electricity_price",
        "heat_pump_coefficient_of_performance",
    )

    hours_of_cooling: Tuple[float, ...]
    hours_of_heating: Tuple[float, ...]
    average_gas_price: Tuple[float, ...]
    average_electricity_price: Tuple[float, ...]
    heat_pump_coefficient_of_performance: Tuple[float, ...]

    def __getstate__(self):
        return dict((name, getattr(self, name)) for name in self.__slots__)

    def __setstate__(self, state):
        for name, value in state.items():
            object.__setattr__(self, name, value)


@dataclass(frozen=True)
class ScenarioOfYearlyTimeSeries:
    __slots__ = ("year_index", "time_series", "average_values")

    year_index: Tuple[int, ...]
    time_series: Tuple[YearlyTimeSeries, ...]
    average_values: YearlyTimeSeries

    def __getstate__(self):
        return dict((name, getattr(self, name)) for name in self.__slots__)

    def __setstate__(self, state):
        for name, value in state.items():
            object.__setattr__(self, name, value)
