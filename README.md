# ROM-TEAM-3

This Repository contains all code we aim to use in the Assignment 2 of the course 
"101-0530-00L Real Options for Infrastructure Management FS2021"

[![Checked with mypy](http://www.mypy-lang.org/static/mypy_badge.svg)](http://mypy-lang.org/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)

To use it, install all dependencies listed in the requirements.txt file:

````
pip install -r requirements.txt
````

Then run the file design analysis (design_analysis.py)

````
python design_analysis.py 
````

You will find the resulting plots in the `out` directory (all html) and some statistics is printed to the console.

For further information, we guide you towards the report.
