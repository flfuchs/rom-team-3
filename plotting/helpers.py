import os
from math import ceil
from typing import Iterable, Tuple, TypeVar


def get_path_to_plots_directory(filename: str) -> str:
    _out_path = os.path.join(os.getcwd(), "out")
    if not os.path.exists(_out_path):
        os.mkdir(_out_path)
    return os.path.join(_out_path, filename)


T = TypeVar("T", str, int, float)


def percentile(data: Iterable[T], percentages: Iterable[float]) -> Tuple[T, ...]:
    sorted_data = sorted(data)
    n = len(sorted_data)
    return tuple(sorted_data[int(ceil((n * p) / 100)) - 1] for p in percentages)
