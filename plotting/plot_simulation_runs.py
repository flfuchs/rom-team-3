from itertools import accumulate
from statistics import mean
from typing import Iterable

import plotly.graph_objects as go

from evaluation.dataclasses.evaluated_design import EvaluatedDesign
from plotting._templates import create_figure_with_template_layout, color_palette


def plot_cumulative_evaluation_values_vs_time(evaluated_runs: EvaluatedDesign) -> go.Figure:
    figure = create_figure_with_template_layout()
    for i, run in enumerate(evaluated_runs.evaluated_time_series):
        figure.add_trace(
            go.Scattergl(
                x=evaluated_runs.year_index,
                y=accumulate(run.evaluation_value),
                mode="lines+markers",
                name=f"run id {i}",
            ),
        )

    figure.update_layout(
        xaxis_title="Time [Years]",
        yaxis_title="Cumulative Evaluation Value [MU]",
    )

    return figure


def plot_evaluation_values_vs_time(evaluated_runs: EvaluatedDesign) -> go.Figure:
    figure = create_figure_with_template_layout()
    for i, run in enumerate(evaluated_runs.evaluated_time_series):
        figure.add_trace(
            go.Scattergl(
                x=evaluated_runs.year_index,
                y=run.evaluation_value,
                mode="lines+markers",
                name=f"run id {i}",
            ),
        )

    figure.update_layout(
        xaxis_title="Time [Years]",
        yaxis_title="Evaluation Value [MU]",
    )
    return figure


symbol_sequence = ['triangle-left',
                   'triangle-right',
                   'triangle-up',
                   'triangle-down',
                   "triangle-ne"]


def plot_cumulated_values_vs_probabilities(
        evaluated_designs: Iterable[EvaluatedDesign], labels: Iterable[str]
) -> go.Figure:
    figure = create_figure_with_template_layout()
    color_counter = 0
    for design, label in zip(evaluated_designs, labels):
        number_of_runs = len(design.evaluated_time_series)
        name = label
        figure.add_trace(
            go.Scattergl(
                y=[100 * i / number_of_runs + 1 / number_of_runs for i in range(number_of_runs)],
                x=sorted(sum(run.evaluation_value) for run in design.evaluated_time_series),
                mode="markers+lines",
                name=name,
                line_color=color_palette[color_counter],  # noqa
                line_shape="hv",  # noqa
                legendgroup=name,
            ),
        )
        figure.add_trace(
            go.Scattergl(
                y=[100, 0],
                x=[mean(sum(run.evaluation_value) for run in design.evaluated_time_series)] * 2,
                line_color=color_palette[color_counter],
                name=name,
                line=dict(dash="dash"),
                mode="lines+markers",
                legendgroup=name,
                showlegend=False,
                marker=dict(line=dict(color='black', width=0.1, )),
            )
        )
        color_counter += 1

    figure.update_layout(
        yaxis_title="cumulative probability [%]",
        xaxis_title="Total Evaluation Value [{0}]".format("\xA3"),
        template="seaborn",
        xaxis_rangemode="tozero",
    )
    figure.update_layout(legend=dict(orientation="h", yanchor="bottom", y=1.02, xanchor="right", x=1))

    return figure


def plot_design_switch_vs_time_per_design(
        evaluated_designs: Iterable[EvaluatedDesign], labels: Iterable[str]
) -> go.Figure:
    figure = create_figure_with_template_layout()
    color_counter = 0
    for design, label in zip(evaluated_designs, labels):
        number_of_runs = len(design.evaluated_time_series)
        name = label
        figure.add_trace(
            go.Scattergl(
                y=[100 * i / number_of_runs + 1 / number_of_runs for i in range(number_of_runs)],
                x=sorted(0 if run.design_switch_triggered_at is None else run.design_switch_triggered_at
                         for run in design.evaluated_time_series),
                mode="markers+lines",
                name=name,
                line_color=color_palette[color_counter],  # noqa
                line_shape="hv",  # noqa
                legendgroup=name,
            ),
        )
        color_counter += 1

    figure.update_layout(
        yaxis_title="cumulative probability [%]",
        xaxis_title="Year of design change [Y]",
        template="seaborn",
        xaxis_rangemode="tozero",
    )

    figure.update_layout(legend=dict(
        orientation="h",
        yanchor="bottom",
        y=1.02,
        xanchor="right",
        x=1
    ))

    return figure
