from collections import defaultdict
from math import log
from statistics import quantiles
from typing import Iterable, Optional, Tuple

import plotly.graph_objects as go
from plotly.subplots import make_subplots

from plotting._templates import _template_font
from plotting.helpers import percentile
from simulation.simulated_time_series import ScenarioOfYearlyTimeSeries

_LABELS = (
    "Heating [h/year]",
    "Cooling [h/year]",
    "Gas Price [{0}/kWh]".format("\xA3"),
    "Electricity Price [{0}/kWh]".format("\xA3"),
    "Heat-Pump COP",
)


def _label_subplots(figure: go.Figure, labels: Tuple[str, ...]) -> None:
    figure.update_layout(template="seaborn", font=_template_font)
    for i, label in enumerate(labels):
        figure.layout[f"xaxis{i + 1}"]["title"] = "year"
        figure.layout[f"yaxis{(i * 2 + 1)}"]["title"] = label
        figure.update_yaxes(
            title="cumulative probability [%]",
            secondary_y=True,
            row=i + 1,
            col=1,
            overwrite=False,
            showticklabels=False,
            showgrid=False,
        )
        figure.update_yaxes(rangemode="tozero", row=i + 1, col=1)


def plot_time_series_individually(simulation_result: ScenarioOfYearlyTimeSeries, include_first=True) -> go.Figure:
    figure = make_subplots(rows=5, cols=1)

    if include_first:
        start = 0
    else:
        start = 1

    x = simulation_result.year_index[start:]
    for i, ts in enumerate(simulation_result.time_series):
        group = f"Run {i}"
        figure.add_trace(go.Scatter(x=x, y=ts.hours_of_heating[start:], legendgroup=group), row=1, col=1)
        figure.add_trace(go.Scatter(x=x, y=ts.hours_of_cooling[start:], legendgroup=group), row=2, col=1)
        figure.add_trace(go.Scatter(x=x, y=ts.average_gas_price[start:], legendgroup=group), row=3, col=1)
        figure.add_trace(go.Scatter(x=x, y=ts.average_electricity_price[start:], legendgroup=group), row=4, col=1)
        figure.add_trace(
            go.Scatter(x=x, y=ts.heat_pump_coefficient_of_performance[start:], legendgroup=group), row=5, col=1
        )

    _label_subplots(figure, _LABELS)

    return figure


def plot_and_show_time_series_individually(simulation_result: ScenarioOfYearlyTimeSeries, include_first=True) -> None:
    plot_time_series_individually(simulation_result, include_first).show()


def plot_probability_intervals(
        time_series: ScenarioOfYearlyTimeSeries, attributes_to_plot: Optional[Iterable[str]] = None
) -> go.Figure:
    if attributes_to_plot is None:
        attributes_to_plot = time_series.time_series[0].__slots__
    else:
        attributes_to_plot = set(time_series.time_series[0].__slots__).intersection(attributes_to_plot)
    figure = make_subplots(
        rows=len(attributes_to_plot), cols=1, specs=[[{"secondary_y": True}]] * len(attributes_to_plot)
    )
    n = int(4 * int(1 + log(len(time_series.time_series))))
    percentiles_to_plot = (2.5, 17, 50, 83, 97.5)
    for index, field in enumerate(attributes_to_plot):
        lower_bounds = defaultdict(list)
        upper_bounds = defaultdict(list)
        all_percentiles = defaultdict(list)
        for year_index in range(1, len(time_series.year_index)):
            values = [ts.__getattribute__(field)[year_index] for ts in time_series.time_series]
            bounds = quantiles(values, n=n)
            percentiles = percentile(values, percentiles_to_plot)

            for j in range(n // 2):
                lower_bounds[j].append(bounds[j])
                upper_bounds[j].append(bounds[-(1 + j)])

            for p_label, p_value in zip(percentiles_to_plot, percentiles):
                all_percentiles[p_label].append(p_value)

        for lower_bound, upper_bound in zip(lower_bounds.values(), upper_bounds.values()):
            figure.add_trace(
                go.Scattergl(
                    x=time_series.year_index[1:] + time_series.year_index[::-1],
                    y=lower_bound + upper_bound[::-1],
                    fill="toself",
                    fillcolor=f"rgba(100, 0, 0, {2 / n})",
                    hoverinfo="skip",
                    line=dict(width=0),
                    showlegend=False,
                ),
                row=index + 1,
                col=1,
            )

        for key, values in all_percentiles.items():
            figure.add_trace(
                go.Scattergl(
                    x=time_series.year_index[1:] + time_series.year_index[::-1],
                    y=values,
                    hoverinfo="skip",
                    line=dict(color="rgba(100, 100, 100, 1)", dash="dash", width=1),
                    showlegend=False,
                ),
                row=index + 1,
                col=1,
            )
            figure.add_trace(
                go.Scattergl(
                    x=[time_series.year_index[-1]], y=[0], hoverinfo="skip", showlegend=False, line=None, mode="lines"
                ),
                secondary_y=True,
                row=index + 1,
                col=1,
            )
            figure.add_annotation(
                dict(
                    xref="paper",
                    x=time_series.year_index[-1],
                    y=values[-1],
                    xanchor="left",
                    yanchor="middle",
                    text=f"{key}%",
                    showarrow=False,
                ),
                row=index + 1,
                col=1,
            )

    labels = tuple(
        _LABELS[i] for i, slot in enumerate(time_series.time_series[0].__slots__) if slot in attributes_to_plot
    )
    _label_subplots(figure, labels)
    return figure.show()
