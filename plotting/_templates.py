import plotly.graph_objects as go

_template_font = dict(family="Helvetica", size=36, color="Black")


class ColorPalette:
    _colors = ("#000000", "#E69F00", "#56B4E9", "#009E73", "#CC79A7", "#0072B2", "#F0E442", "#D55E00",)

    @classmethod
    def __getitem__(cls, item) -> str:
        return cls._colors[item % len(cls._colors)]


color_palette = ColorPalette()


def create_figure_with_template_layout() -> go.Figure:
    figure = go.Figure()
    figure.update_layout(
        font=_template_font,
    )
    return figure
